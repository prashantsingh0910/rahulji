<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <custom>true</custom>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AccountBrand-Account BrandLayout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AccountContactRelation-Account Contact Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account__c-Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Asset-Asset Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AssetRelationship-Asset Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Bank_Account__c-Bank Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Branch__c-Branch Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Campaign-Campaign Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMember-Campaign Member Page Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-Close Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseMilestone-Case Milestone Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-Content Version Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContractLineItem-Contract Line Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract__c-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Customer1__c-Customer1 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Customer__c-Customer Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DandBCompany-D%26B Company Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Drinks_List__c-Drinks List Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Drinks_Order__c-Drinks Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DuplicateRecordSet-Duplicate Record Set Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EmailMessage-Email Message Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Employee__c-Employee Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Entitlement-Entitlement Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EntityMilestone-Object Milestone Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event-Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FeedItem-Feed Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Global-Global Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Goal-Goal Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>GoalLink-Goal Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Hospital__c-Hospital Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Hospitals__c-Hospital Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Hotel__c-Hotel Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Idea-Idea Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>JobTracker-Job Tracker Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Job_Posting1__c-Job Posting Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Job_Posting_Site__c-Job Posting Site Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Knowledge__kav-Knowledge Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Lead-Lead Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveAgentSession-Live Agent Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscript-Live Chat Transcript Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptActive-Live Chat Transcript %28In Progress%29 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptEvent-Live Chat Transcript Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptWaiting-Live Chat Transcript %28Waiting%29 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatVisitor-Live Chat Visitor Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Macro-Macro Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Metric-Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>MetricDataLink-Metric Data Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>My_First_Knowledge_Article__kav-My First Knowledge Article Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Offer__c-Offer Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-Opportunity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityTeamMember-Opportunity Team Member Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order-Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OrderItem-Order Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Position__c-Position Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PricebookEntry-Price Book Entry Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Project__c-Project Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Property__c-Property Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuickText-Quick Text Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Quote-Quote Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuoteLineItem-Quote Line Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Review__c-Review Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Room__c-Room Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Scorecard-Scorecard Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardAssociation-Scorecard Association Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardMetric-Scorecard Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServiceContract-Service Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SessionName__c-Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Session_Speaker1__c-Session Speaker Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPersona-Social Persona Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPost-Social Post Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Solution-Solution Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Speaker__c-Speaker Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Suggestion__c-Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Task-Task Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Threat_Tier__mdt-Threat Tier Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Trainingg__c-Trainingg Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Transaction__c-Transaction Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>User-User Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAlt-User Profile Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvAccount-User Provisioning Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningLog-User Provisioning Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningRequest-User Provisioning Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkCoaching-Coaching Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedback-Feedback Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestion-Feedback Question Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestionSet-Feedback Question Set Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackRequest-Feedback Request Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackTemplate-Feedback Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkPerformanceCycle-Performance Cycle Layout - Winter %2716</layout>
    </layoutAssignments>
    <pageAccesses>
        <apexPage>AccountDisplay</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>AccountSearch</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>AnswersHome</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>BandwidthExceeded</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>ChangePassword</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>CommunitiesLanding</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>CommunitiesLogin</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>CommunitiesSelfReg</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>CommunitiesSelfRegConfirm</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>CommunitiesTemplate</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>ContactsListController</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>Exception</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>FileNotFound</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>ForgotPassword</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>ForgotPasswordConfirm</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>HelloWorld</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>IdeasHome</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>InMaintenance</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>MyPage</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>MyPagePDF</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>MyProfilePage</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>NewAccount</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>SiteLogin</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>SiteRegister</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>SiteRegisterConfirm</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>SiteTemplate</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>StdExceptionTemplate</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>Unauthorized</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>UnderConstruction</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>VF1</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>VF3</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <pageAccesses>
        <apexPage>VF4</apexPage>
        <enabled>false</enabled>
    </pageAccesses>
    <tabVisibilities>
        <tab>Account__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Bank_Account__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Branch__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Customer1__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Customer__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Drinks_List__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Drinks_Order__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Employee__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Hospitals__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Hotel__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Job_Posting1__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Job_Posting_Site__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Position__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Review__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Room__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>SessionName__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Speaker__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Suggestion__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Trainingg__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Transaction__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <userLicense>Guest</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>AddDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowUniversalSearch</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ContentWorkspaces</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableCommunityAppLauncher</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableNotifications</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RemoveDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SelectFilesFromSalesforce</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>UseWebLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewAllUsers</name>
    </userPermissions>
</Profile>
